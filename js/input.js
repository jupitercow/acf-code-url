(function($){
	
	
	/*
	*  acf/setup_fields
	*
	*  This event is triggered when ACF adds any new elements to the DOM. 
	*
	*  @type	function
	*  @since	1.0.0
	*  @date	01/01/12
	*
	*  @param	event		e: an event object. This can be ignored
	*  @param	Element		postbox: An element which contains the new HTML
	*
	*  @return	N/A
	*/
	
	$(document).live('acf/setup_fields', function(e, postbox){
		
		$(postbox).find('.code_url').each(function(){

			var $this = $(this);
			$this.val( $this.data('url') + '/' + $this.parent().siblings('.field_type-code').children('input').val() + '/' );

		});
	
	});

})(jQuery);
