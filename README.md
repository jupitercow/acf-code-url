# ACF { Code URL

Adds a readonly field that can be used with ACF Code to create URL containing the code. This could be used as a password url or coupon url.


### Compatibility

This add-on will work with:

* version 4 and up
* NOT version 3 and bellow


### Installation

This add-on can be treated as both a WP plugin.

**Install as Plugin**

1. Copy the folder into your plugins folder
2. Activate the plugin via the Plugins admin page